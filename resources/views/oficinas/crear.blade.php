@extends('layout')
@section('titulo', 'Crear')

@section('contenido')
<div class="container">
	<a class="btn btn-dark" href="{{url('oficinas')}}">Regresar</a>
</div>

	<form action="{{url('oficinas')}}" method="post">
		@csrf()
		<input class="form-control" name="nombre" placeholder="nombre" />
        <input class="form-control" name="pais" placeholder="pais" />
        <input class="form-control" name="departamento" placeholder="departamento" />
        <input class="form-control" name="provincia" placeholder="provincia" />
        <input class="form-control" name="distrito" placeholder="distrito" />
        <input class="form-control" name="telefono" placeholder="telefono" />
		<select class="custom-select" name="estado">
			<option value="0">Inactivo</option>
			<option value="1">Activo</option>
		</select>
		<input class="btn btn-primary" type="submit" value="Ingresar" />
	</form>
	
	
@endsection