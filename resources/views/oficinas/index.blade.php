@extends('layout')
@section('titulo', 'Listar')
@section('contenido')
<a class="btn btn-secondary" href="{{url('oficinas/create')}}">Nuevo</a>
	<table class="table table-sm table-dark table-striped table-hover table-bordered">
		<thead>
			<tr>
				<th>Código</th>
				<th>nombre</th>
				<th>pais</th>
				<th>departamento</th>
				<th>provincia</th>
				<th>distrito</th>
				<th>telefono</th>
				<th>estado de oficina</th>
			</tr>
		</thead>
		<tbody>
			@foreach($oficinas as $oficina)
			<tr>
				<td>{{ $oficina->id }}</td>
				<td>{{ $oficina->nombre }}</td>
				<td>{{ $oficina->pais }}</td>
				<td>{{ $oficina->departamento }}</td>
				<td>{{ $oficina->provincia }}</td>
				<td>{{ $oficina->distrito }}</td>
				<td>{{ $oficina->telefono }}</td>
                @if ($oficina->estado == 1)
                <td>Habilitado</td>
                @else
                <td>Deshabilitado</td>
                @endif
				<td>
					<a class="btn btn-warning" href="{{url('oficinas/' . $oficina->id. '/edit')}}">Editar</a>
				</td>
				<td>
					<form method="post" action="{{url('oficinas', $oficina->id)}}">
						@csrf()
						@method('DELETE')
						<input class="btn btn-danger" type="submit" value="Eliminar" />
					</form>
				</td>

			</tr>
			@endforeach
		</tbody>
	</table>
@endsection