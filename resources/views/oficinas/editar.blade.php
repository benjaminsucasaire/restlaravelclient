@extends('layout')
@section('titulo', 'Editar')

@section('contenido')
<div class="container">
	<a class="btn btn-dark" href="{{url('oficinas')}}">Regresar</a>
</div>

	<form action="{{url('oficinas',$oficina->id)}}" method="post">
		@csrf()
        @method('POST')
		<input class="form-control" name="nombre" placeholder="nombre" required value="{{$oficina->nombre}}" />
        <input class="form-control" name="pais" placeholder="pais" required value="{{$oficina->pais}}" />
        <input class="form-control" name="departamento" placeholder="departamento" required value="{{$oficina->departamento}}"/>
        <input class="form-control" name="provincia" placeholder="provincia" required value="{{$oficina->provincia}}"/>
        <input class="form-control" name="distrito" placeholder="distrito" required value="{{$oficina->distrito}}"/>
        <input class="form-control" name="telefono" placeholder="telefono" required value="{{$oficina->telefono}}"/>
		<select class="custom-select" name="estado">
            <option  value="{{$oficina->estado}}"> -- estado de oficina -- </option>
			<option value="0">Deshabilitado</option>
			<option value="1">Habilitado</option>
		</select>
		<input class="btn btn-success" type="submit" value="Editar" />
	</form>
	
	
@endsection