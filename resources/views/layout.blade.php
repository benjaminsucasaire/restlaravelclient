<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>Oficina - @yield('titulo')</title>
	<link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}" />
</head>
<body>
	<div class="container">
		<div class="row">
			<header>
				<h1>Oficina</h1>
				<h2>@yield('titulo')</h2>
			</header>
		</div>
		<div class="row">
			@yield('contenido')
		</div>
	</div>
</body>
</html>