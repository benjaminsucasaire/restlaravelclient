<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\OficinaController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('oficinas', OficinaController::class);
Route::post('oficinas/{oficina}',[ OficinaController::class,'actualizar'])->name('oficinas.actualizar');
